var _ = require('lodash')
  , _str = require('underscore.string')
  , SqlModel = require('../models/SqlModel')
  , rangeParser = require('../http/range-parser')
  , ValidationError = require('../errors/ValidationError')
  , EagerExpression = require('../models/SqlModel/EagerExpression');

/**
 * Helper class for creating complex find queries for an `SqlModel`.
 *
 * This class allows the result to be paged, filtered, searched, ordered and even joined with
 * related models. Everything is controlled by query parameters and headers.
 *
 * Most query parameters take so called property references as values. Property references have the
 * form `[<relationName>]<propertyName>` with camel casing. For example the `age` property of the
 * `wine` relation is referenced by `wineAge`. The `name` property of the model itself is referenced
 * simply by `name`. If the model itself has a property `wineAge` it is selected instead. So the
 * model's own properties take precedence when a name collision happens.
 *
 * The accepted query parameters are:
 *
 * ```
 * join=<propertyRef>
 * searchBy=<propertyRef>
 * orderBy=<propertyRef>
 * <propertyRef>=string
 * orderDesc=true/false
 * eager=<eagerDotNotation>
 * search=<sqlLikePattern>
 * ```
 *
 * Accepted headers:
 *
 * ```
 * Range: resources=<first>-<last>
 * ```
 *
 * There can be multiple instances of all query parameters except `orderDesc`, `eager` and `search`. The
 * `Range` header follows the HTTP specification and writes a `Content-Range: resources <first>-<last>/<total>`
 * header to the response. The search string can be any pattern accepted by SQL `LIKE` operator.
 *
 * For security reasons all property references must be explicitly allowed. If a unallowed property
 * reference is encountered a `ValidationError` is thrown. The `findQuery` builder has methods `allowJoin()`,
 * `allowSearch()`, `allowOrderBy()` and `allowFilter()` that must be called with allowed property references
 * as values. There is also the `allowEager()` method that sets the allowed relation tree that can be fetched
 * using the `eager` query parameter. Each of these methods accept a list of arguments or an array. See the
 * examples below.
 *
 * Example 1:
 *
 * Request:
 *
 * ```
 * GET /some/path ? join=parentName & search=%jennifer% & searchBy=parentName
 * ```
 *
 * Handler:
 *
 * ```js
 * router
 *   .get('/some/path')
 *   .handler(function (req, res) {
 *     return findQuery
 *       .builder(req.models.SomeModel, req, res)
 *       .allowJoin('parentName')
 *       .allowSearch('parentName')
 *       .build();
 *   });
 * ```
 *
 * Result is all instances of `SomeModel` whose `parent` relation's `name` attribute matches the filter
 * '%jennifer%'. Here we assume that `SomeModel` has a has-one relation named `parent` and the related
 * model has a `name` property. `findQuery` automatically joins the the `parent` relation and selects
 * the `name` property and adds it to the resulting objects as `parentName` property.
 *
 * Example 2:
 *
 * Request:
 *
 * ```
 * GET /some/path ? type=Actor & search=%damon & searchBy=name & searchBy=parentName & orderBy=parentName & eager=[parent, otherRelation.[sub1, sub2]]
 * ```
 *
 * Handler:
 *
 * ```js
 * router
 *   .get('/some/path')
 *   .handler(function (req, res, transaction) {
 *     return findQuery
 *       .builder(req.models.SomeModel, req, res)
 *       .allowPaging(true)
 *       .allowFilter('type')
 *       .allowJoin('parentName')
 *       .allowSearch('name', 'parentName')
 *       .allowOrderBy('name', 'parentName')
 *       .transacting(transaction)
 *       .allowEager('[parent.*, otherRelation.*]')
 *       .build(function (queryBuilder) {
 *         queryBuilder.dumpSql();
 *       });
 *   });
 * ```
 *
 * In this example we have multiple `searchBy` query parameters so we also need to allow them all. There
 * is also the allowed filter `type`. The filters are tested with strict equality instead of pattern
 * matching. So only rows with `type === 'Actor'` will be returned. The `build()` method takes an
 * optional function in which the query can be further modified.
 *
 * @constructor
 */
function FindQueryBuilder(modelClass, req, res) {
  this._modelClass = modelClass;
  this._req = req;
  this._res = res;
  this._allowedSearches = [];
  this._defaultSearches = [];
  this._allowedFilters = [];
  this._allowedJoins = [];
  this._allowedOrderBy = [];
  this._defaultOrderBy = [];
  this._searchMethods = {};
  this._allowPaging = true;
  this._allowEager = null;
  this._transaction = null;
}

/**
 * @param {SqlModel} modelClass
 * @param {Request} req
 * @param {ServerResponse} res
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.builder = function (modelClass, req, res) {
  return new FindQueryBuilder(modelClass, req, res);
};

/**
 * @param {Boolean} allowPaging
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.allowPaging = function (allowPaging) {
  this._allowPaging = allowPaging;
  return this;
};

/**
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.allowSearch = function () {
  this._allowedSearches = toArray(arguments);
  return this;
};

/**
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.defaultSearch = function () {
  this._defaultSearches = toArray(arguments);
  return this;
};

/**
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.allowFilter = function () {
  this._allowedFilters = toArray(arguments);
  return this;
};

/**
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.allowJoin = function () {
  this._allowedJoins = toArray(arguments);
  return this;
};

/**
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.allowOrderBy = function () {
  this._allowedOrderBy = toArray(arguments);
  return this;
};

/**
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.defaultOrderBy = function () {
  this._defaultOrderBy = toArray(arguments);
  return this;
};

/**
 * @param {Transaction} transaction
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.transacting = function (transaction) {
  this._transaction = transaction;
  return this;
};

/**
 * @param {String} allowEager
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.allowEager = function (allowEager) {
  this._allowEager = allowEager;
  return this;
};

/**
 * @param {String} propertyRef
 * @param {Function} method
 * @returns {FindQueryBuilder}
 */
FindQueryBuilder.prototype.searchMethod = function (propertyRef, method) {
  this._searchMethods[propertyRef] = method;
  return this;
};

/**
 * @param {function(SqlModelQueryBuilder)=} editQuery
 * @returns {Promise}
 */
FindQueryBuilder.prototype.build = function (editQuery) {
  var searchBy = this._createSearchByProperties();
  var orderBy = this._createOrderByProperties();
  var filters = this._createFilterProperties();
  var joins = this._createJoinProperties(searchBy, orderBy, filters);

  var queryBuilder = this._modelClass.find().transacting(this._transaction);

  this._buildEager(queryBuilder);
  this._buildJoins(queryBuilder, joins);
  this._buildSearch(queryBuilder, searchBy);
  this._buildFilters(queryBuilder, filters);
  this._buildOrderBy(queryBuilder, orderBy);

  if (_.isFunction(editQuery)) {
    editQuery(queryBuilder);
  }

  return this._buildPaging(queryBuilder);
};

/**
 * @private
 * @returns {Array.<Property>}
 */
FindQueryBuilder.prototype._createSearchByProperties = function () {
  var self = this;

  return _.map(toUniqueArray(this._req.query.searchBy || this._defaultSearches), function (searchBy) {
    if (!_.contains(self._allowedSearches, searchBy)) {
      throw propertyError(searchBy, 'search is not allowed by this property');
    }
    return Property.parse(searchBy, self._modelClass);
  });
};

/**
 * @private
 * @returns {Array.<Property>}
 */
FindQueryBuilder.prototype._createOrderByProperties = function () {
  var self = this;

  return _.map(toUniqueArray(this._req.query.orderBy || this._defaultOrderBy), function (orderBy) {
    if (!_.contains(self._allowedOrderBy, orderBy)) {
      throw propertyError(orderBy, 'ordering by this property is not allowed');
    }
    return Property.parse(orderBy, self._modelClass);
  });
};

/**
 * @private
 * @returns {Array.<Property>}
 */
FindQueryBuilder.prototype._createFilterProperties = function () {
  var self = this;

  return _.reduce(this._allowedFilters, function (result, filter) {
    if (!self._req.query[filter]) {
      return result;
    }
    result.push(Property.parse(filter, self._modelClass));
    return result;
  }, []);
};

/**
 * @private
 * @returns {Array.<Property>}
 */
FindQueryBuilder.prototype._createJoinProperties = function (searchBy, orderBy, filters) {
  var self = this;

  var explicitJoins = _.map(toUniqueArray(this._req.query.join), function (joinProperty) {
    return Property.parse(joinProperty, self._modelClass);
  });

  return _.reduce(toArray(orderBy, searchBy, filters, explicitJoins), function (result, joinProperty) {
    if (!joinProperty.needsJoin()) {
      return result;
    }

    if (!_.contains(self._allowedJoins, joinProperty.name())) {
      throw propertyError(joinProperty.name(), 'join not allowed');
    }

    result[joinProperty.relationName] = result[joinProperty.relationName] || [];

    if (!_.find(result[joinProperty.relationName], {columnName: joinProperty.columnName})) {
      result[joinProperty.relationName].push(joinProperty);
    }

    return result;
  }, {});
};

/**
 * @private
 */
FindQueryBuilder.prototype._buildEager = function (queryBuilder) {
  if (this._req.query.eager) {
    var allowEager = new EagerExpression(this._allowEager);

    if (_.isArray(this._req.query.eager)) {
      throw new ValidationError({eager: 'only one eager query parameter is accepted'});
    }

    if (!allowEager.isSubTree(this._req.query.eager)) {
      throw new ValidationError({eager: 'eager query not allowed'});
    }

    queryBuilder.eager(this._req.query.eager);
  }
};

/**
 * @private
 */
FindQueryBuilder.prototype._buildJoins = function (queryBuilder, joins) {
  var self = this;
  var select = [this._modelClass.tableName + '.*'];

  _.forEach(joins, function (joinProperties) {
    var foreignKey = joinProperties[0].foreignKey();
    var tableName = joinProperties[0].tableName();
    var tableAlias = joinProperties[0].tableAlias();

    queryBuilder.leftJoin(tableName + ' as ' + tableAlias,  self._modelClass.tableName + '.' + foreignKey,  tableAlias + '.id');

    _.forEach(joinProperties, function (joinProp) {
      select.push(joinProp.fullColumnName() + ' as ' + joinProp.name());
    });
  });

  queryBuilder.select(select);
};

/**
 * @private
 */
FindQueryBuilder.prototype._buildSearch = function (queryBuilder, searchBy) {
  var self = this;
  var formatter = this._modelClass.dbFormatter();
  var search = this._req.query.search;

  if (searchBy.length && search && search !== '%' && search !== '%%') {
    queryBuilder.where(function () {
      var queryBuilder = this;
      _.forEach(searchBy, function (searchByProperty) {
        var searchMethod = self._searchMethods[searchByProperty.name()] || defaultSearchMethod;
        searchMethod(queryBuilder, searchByProperty.fullColumnName(), search, formatter);
      });
    });
  }
};

/**
 * @private
 */
FindQueryBuilder.prototype._buildFilters = function (queryBuilder, filters) {
  var self = this;

  _.forEach(filters, function (filterProperty) {
    var value = self._req.query[filterProperty.name()];
    if (_.isArray(value)) {
      queryBuilder.whereIn(filterProperty.fullColumnName(), value);
    } else {
      queryBuilder.where(filterProperty.fullColumnName(), value);
    }
  });
};

/**
 * @private
 */
FindQueryBuilder.prototype._buildOrderBy = function (queryBuilder, orderBy) {
  var self = this;

  _.forEach(orderBy, function (orderBy) {
    queryBuilder.orderBy(orderBy.fullColumnName(), self._req.query.orderDesc === 'true' ? 'desc' : 'asc');
  });
};

/**
 * @private
 * @returns {Promise}
 */
FindQueryBuilder.prototype._buildPaging = function (queryBuilder) {
  if (this._allowPaging) {
    var range = rangeParser(this._req, this._res);
    if (range) {
      queryBuilder = queryBuilder.range(range.start, range.end).then(function (result) {
        range.writeContentRange(result.total);
        return result.results;
      });
    }
  }

  return queryBuilder;
};

/**
 * @constructor
 */
function Property(columnName, relationName, modelClass) {
  this.columnName = columnName;
  this.relationName = relationName;
  this.modelClass = modelClass;
}

/**
 * @returns {Property}
 */
Property.parse = function (propertyStr, modelClass) {
  // First test the property against the model class's own properties.
  if (modelClass.schema.properties[propertyStr]) {
    return new Property(propertyStr, null, modelClass);
  }

  var property = null;
  // Then test if the property string starts with a relation name.
  _.forEach(modelClass.getRelations(), function (relation, relationName) {
    if (relation.constructor !== SqlModel.HasOneRelation) {
      // Can only join has-one relations.
      return;
    }
    if (_str.startsWith(propertyStr, relationName)) {
      var columnName = _str.decapitalize(propertyStr.substring(relationName.length));

      // Make sure the related model has the property.
      if (!relation.relatedModelClass.schema.properties[columnName]) {
        throw propertyError(propertyStr);
      }

      property = new Property(columnName, relationName, modelClass);
      return false;
    }
  });

  if (property === null) {
    throw propertyError(propertyStr);
  }

  return property;
};

/**
 * @returns {Boolean}
 */
Property.prototype.needsJoin = function () {
  return this.relationName !== null;
};

/**
 * @returns {String}
 */
Property.prototype.name = function () {
  if (this.relationName) {
    return this.relationName + _str.capitalize(this.columnName);
  } else {
    return this.columnName;
  }
};

/**
 * @returns {String}
 */
Property.prototype.tableName = function () {
  if (this.relationName) {
    return this.modelClass.getRelation(this.relationName).relatedModelClass.tableName;
  } else {
    return this.modelClass.tableName;
  }
};

/**
 * @returns {String}
 */
Property.prototype.tableAlias = function () {
  var tableName = this.tableName();
  if (this.relationName) {
    return _str.capitalize(this.relationName) + tableName;
  } else {
    return tableName;
  }
};

/**
 * @returns {String}
 */
Property.prototype.fullColumnName = function () {
  return this.tableAlias() + '.' + this.columnName;
};

/**
 * @returns {String}
 */
Property.prototype.foreignKey = function () {
  return this.modelClass.getRelation(this.relationName).ownerJoinColumn;
};

/**
 * @private
 */
function toArray() {
  return _(arguments).flatten().compact().value();
}

/**
 * @private
 */
function toUniqueArray() {
  return _.unique(toArray(arguments));
}

/**
 * @private
 */
function propertyError(propertyStr, message) {
  var data = {};
  data[propertyStr] = message || 'invalid property';
  return new ValidationError(data);
}

/**
 * @private
 */
function defaultSearchMethod(queryBuilder, columnName, search, formatter) {
  queryBuilder.orWhereRaw('lower(' + formatter.wrap(columnName) + ') like ?', [search.toLowerCase()]);
}

module.exports = FindQueryBuilder;
