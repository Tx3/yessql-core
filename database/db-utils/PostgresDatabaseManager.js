var _ = require('lodash')
  , pg = require('pg')
  , escape = require('pg-escape')
  , Promise = require('bluebird')
  , classUtils = require('../../class-utils')
  , DatabaseManager = require('./DatabaseManager');

/**
 * @constructor
 */
function PostgresDatabaseManager() {
  DatabaseManager.apply(this, arguments);
  this.masterClient_ = null;
  this.cachedTableNames_ = null;
}

classUtils.inherits(PostgresDatabaseManager, DatabaseManager);

/**
 * @Override
 */
PostgresDatabaseManager.prototype.createDb = function(databaseName) {
  databaseName = databaseName || this.config.database;
  var collate = this.config.collate;
  var self = this;
  var promise = Promise.reject();
  // Try to create with each collate. Use the first one that works. This is kind of a hack
  // but seems to be the only reliable way to make this work with both windows and unix.
  _.each(collate, function(locale) {
    promise = promise.catch(function() {
      return self.masterQuery_("CREATE DATABASE %I ENCODING = 'UTF-8' LC_COLLATE = %L TEMPLATE template0", [databaseName, locale])
    });
  });
  return promise;
};

/**
 * Drops database with name if db exists.
 *
 * @Override
 */
PostgresDatabaseManager.prototype.dropDb = function(databaseName) {
  databaseName = databaseName || this.config.database;
  return this.masterQuery_("DROP DATABASE IF EXISTS %I", [databaseName]);
};

/**
 * @Override
 */
PostgresDatabaseManager.prototype.copyDb = function(fromDatabaseName, toDatabaseName) {
  return this.masterQuery_("CREATE DATABASE %I template %I", [toDatabaseName, fromDatabaseName]);
};

/**
 * @Override
 */
PostgresDatabaseManager.prototype.truncateDb = function(databaseName) {
  var knex = this.knexInstance(databaseName);
  var config = this.config;

  if (!this.cachedTableNames_) {
    this.cachedTableNames_ = knex('pg_tables').select('tablename').where('schemaname', 'public').then(function (tables) {
      return _.without(_.pluck(tables, 'tablename'), config.migrationsTable);
    });
  }

  return this.cachedTableNames_.then(function (tableNames) {
    if (!_.isEmpty(tableNames)) {
      return knex.raw('TRUNCATE TABLE "' + tableNames.join('","') + '" RESTART IDENTITY');
    }
  }).finally(function() {
    knex.destroy();
  });
};

/**
 * @Override
 */
PostgresDatabaseManager.prototype.close = function() {
  if (this.masterClient_) {
    return this.masterClient_.then(function(client) {
      client.end();
    });
  } else {
    return Promise.resolve();
  }
};

/**
 * @private
 * @returns {Promise}
 */
PostgresDatabaseManager.prototype.masterQuery_ = function(query, params) {
  if (!this.masterClient_) {
    this.masterClient_ = this.createMasterClient_();
  }
  var self = this;
  return this.masterClient_.then(function(client) {
    return self.performMasterQuery_(client, query, params);
  });
};

/**
 * @private
 * @returns {Promise}
 */
PostgresDatabaseManager.prototype.createMasterClient_ = function() {
  var self = this;
  return new Promise(function(resolve, reject) {
    var client = new pg.Client(self.masterConnectionUrl_());
    client.connect(function(err) {
      if (err) {
        reject(err);
      } else {
        resolve(client);
      }
    });
  });
};

/**
 * @private
 * @returns {Promise}
 */
PostgresDatabaseManager.prototype.performMasterQuery_ = function(client, query, params) {
  return new Promise(function(resolve, reject) {
    if (params) {
      var args = [query].concat(params);
      query = escape.apply(global, args);
    }
    client.query(query, function(err, result) {
      if (err) {
        reject(err);
      } else {
        resolve(result);
      }
    });
  });
};

/**
 * @private
 * @returns {String}
 */
PostgresDatabaseManager.prototype.masterConnectionUrl_ = function() {
  var url = 'postgres://';
  if (this.config.superUser) {
    url += this.config.superUser;
  } else {
    throw new Error('DatabaseManager: database config must have `superUser`');
  }
  if (this.config.superPassword) {
    url += ':' + this.config.superPassword
  }
  var port = this.config.port || 5432;
  url += '@' + this.config.host + ':' + port + '/template1';
  return url;
};

module.exports = PostgresDatabaseManager;
