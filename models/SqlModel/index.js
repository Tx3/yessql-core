"use strict";

var _ = require('lodash')
  , uuid = require('node-uuid')
  , Promise = require('bluebird');

var Model = require('./../Model')
  , classUtils = require('../../class-utils')
  , arrayUtils = require('../../utils/array-utils')
  , SqlModelQueryBuilder = require('./SqlModelQueryBuilder');

var SqlModelRelation = require('./relations/SqlModelRelation')
  , HasOneSqlModelRelation = require('./relations/HasOneSqlModelRelation')
  , HasManySqlModelRelation = require('./relations/HasManySqlModelRelation')
  , ManyToManySqlModelRelation = require('./relations/ManyToManySqlModelRelation');

/**
 * SqlModel represents an SQL database table.
 *
 * Instances of this class represent rows in the table. Just create a subclass and set the static property
 * `tableName` and you are good to go. Relations to other SqlModels can be defined by setting the static
 * `relationMappings` property. See the documentation of `tableName` and `relationMappings` for more info.
 *
 * This class has methods like `insert`, `update` and `find` for accessing and manipulating the table.
 * Each database method returns an instance of `SqlModelQueryBuilder` that can be used just like a *knex.js*
 * query builder. There are also methods like `$findRelated` and `$insertRelated` for accessing and manipulating
 * models related through relations defined in `relationMappings`.
 *
 * By default `SqlModel` subclasses are not bound to any database. You must either set the `SqlModel.db`
 * property or use `SqlModel.bindDb(knex)` to create bound versions of the model classes. If you only
 * use one database connection, setting the `SqlModel.db` property before creating any subclasses is
 * sufficient.
 *
 * Examples:
 *
 * ```js
 * var SqlModel = require('yessql-core/models/SqlModel');
 * var classUtils = require('yessql-core/class-utils');
 *
 * function Person() {
 *   SqlModel.call(this);
 * }
 *
 * classUtils.inherits(Person, SqlModel);
 *
 * Person.tableName = 'Person';
 * ```
 *
 * ```js
 * Person
 *   .find()
 *   .where('age', '<=', 20)
 *   .andWhere('age', '>=', 12)
 *   .then(function (teenagers) {
 *     _.each(teenagers, function (teenager) {
 *       console.log(teenager.name, teenager.age);
 *     });
 *   });
 *
 * Person
 *  .insert({name: 'Lars', age: 40})
 *  .then(function (insertedPerson) {
 *    console.log(insertedPerson.id);
 *    return insertedPerson.$insertRelated('children', {name: 'Ulf', age: 13});
 *  })
 *  .then(function (insertedChild) {
 *    console.log(insertedChild.name); // --> prints 'Ulf'
 *  });
 * ```
 *
 * @extends Model
 * @constructor
 */
function SqlModel() {
  Model.call(this);
}

classUtils.inherits(SqlModel, Model);

/**
 * Inserts this model to the database.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$insert = function () {
  return this.constructor.insert(this);
};

/**
 * Saves this model to the database.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$update = function () {
  return this.constructor.update(this);
};

/**
 * Deletes this model from the database.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$del = function () {
  return this.constructor.deleteById(this.id);
};

/**
 * Alias for `$del()`.
 *
 * @see $del
 */
SqlModel.prototype.$delete = function () {
  return this.$del();
};

/**
 * Creates a new knex.js query builder for this model class.
 *
 * @return {QueryBuilder}
 */
SqlModel.prototype.$dbQuery = function () {
  return this.constructor.dbQuery();
};

/**
 * Finds models that are related through `relationName` and saves them to `this[relationName]`.
 *
 * @param {String} relationName
 *    Name of the relation.
 *
 * @return {SqlModelQueryBuilder}
 *
 * @see relationMappings
 */
SqlModel.prototype.$findRelated = function (relationName) {
  var relation = this.constructor.getRelation(relationName);
  return relation.find(this);
};

/**
 * Finds one model that is related through `relationName` and saves it to `this[relationName]`.
 *
 * @param {String} relationName
 *    The name of the relation.
 *
 * @return {SqlModelQueryBuilder}
 *
 * @see relationMappings
 */
SqlModel.prototype.$findOneRelated = function (relationName) {
  var relation = this.constructor.getRelation(relationName);
  return relation.findOne(this);
};

/**
 * Inserts a related model to database and binds it to this object.
 *
 * @param {String} relationName
 *    The name of the relation.
 *
 * @param {SqlModel|Object} modelOrJson
 *    An SqlModel instance or a JSON object from which a model can be created.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$insertRelated = function (relationName, modelOrJson) {
  var relation = this.constructor.getRelation(relationName);
  var model = relation.relatedModelClass.ensureModel(modelOrJson);
  return relation.insert(this, model);
};

/**
 * Updates a related model.
 *
 * @param {String} relationName
 *    The name of the relation.
 *
 * @param {SqlModel|Object} modelOrJson
 *    An SqlModel instance or a JSON object to update.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$updateRelated = function (relationName, modelOrJson) {
  var relation = this.constructor.getRelation(relationName);
  var model = relation.relatedModelClass.ensureModel(modelOrJson);
  return relation.update(this, model);
};

/**
 * Deletes a related model from database and removes any bindings to this object.
 *
 * @param {String} relationName
 *    The name of the relation.
 *
 * @param {Number|String=} idOfModelToDelete
 *    Identifier of the model to delete. This is optional for relations that can
 *    only relate one object (like HasOneRelation).
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$delRelated = function (relationName, idOfModelToDelete) {
  var relation = this.constructor.getRelation(relationName);
  return relation.del(this, idOfModelToDelete);
};

/**
 * Alias for `$delRelated`.
 *
 * @see $delRelated
 */
SqlModel.prototype.$deleteRelated = function () {
  return this.$delRelated.apply(this, arguments);
};

/**
 * Binds an existing model to this object through relation ´relationName´.
 *
 * @param {String} relationName
 *    The name of the relation.
 *
 * @param {Number|String} idOfModelToBind
 *    Identifier of the model to bind.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$bindRelated = function (relationName, idOfModelToBind) {
  var relation = this.constructor.getRelation(relationName);
  return relation.bind(this, idOfModelToBind);
};

/**
 * Unbinds a related model from relation ´relationName´.
 *
 * @param {String} relationName
 *    The name of the relation.
 *
 * @param {Number|String=} idOfModelToUnBind
 *    Identifier of the model to unbind. This is optional for relations that can
 *    only relate one object (like HasOneRelation).
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.prototype.$unbindRelated = function (relationName, idOfModelToUnBind) {
  var relation = this.constructor.getRelation(relationName);
  return relation.unbind(this, idOfModelToUnBind);
};

/**
 * @override
 */
SqlModel.prototype.$parseDatabaseJson = function (json) {
  var ModelClass = this.constructor;
  var jsonAttr = ModelClass.jsonAttributes;
  if (jsonAttr.length) {
    for (var i = 0, l = jsonAttr.length; i < l; ++i) {
      var attr = jsonAttr[i];
      var value = json[attr];
      if ('string' === typeof value) {
        json[attr] = JSON.parse(value);
      }
    }
  }
  return json;
};

/**
 * @override
 */
SqlModel.prototype.$formatDatabaseJson = function (json) {
  var ModelClass = this.constructor;
  var jsonAttr = ModelClass.jsonAttributes;
  if (jsonAttr.length) {
    for (var i = 0, l = jsonAttr.length; i < l; ++i) {
      var attr = jsonAttr[i];
      var value = json[attr];
      if ('object' === typeof value) {
        json[attr] = JSON.stringify(value);
      }
    }
  }
  return ModelClass.omitNonColumns_(json);
};

/**
 * @override
 */
SqlModel.prototype.$parseJson = function (json) {
  var relations = this.constructor.getRelations();
  if (relations.length === 0) {
    return json;
  }
  // Parse relations into SqlModel instances and remove them from the json.
  for (var relationName in relations) {
    if (relationName in json) {
      var relationJson = json[relationName];
      var relation = relations[relationName];
      if (Array.isArray(relationJson)) {
        var arr = new Array(relationJson.length);
        for (var i = 0, l = relationJson.length; i < l; ++i) {
          arr[i] = relation.relatedModelClass.fromJson(relationJson[i]);
        }
        json[relationName] = arr;
      } else if (relationJson) {
        json[relationName] = relation.relatedModelClass.fromJson(relationJson);
      }
    }
  }
  return json;
};

/**
 * @override
 * @param {Boolean} shallow
 *    If true the relations are omitted from the json.
 */
SqlModel.prototype.$toJson = function (shallow) {
  var json = Model.prototype.$toJson.call(this);
  if (shallow) {
    return this.constructor.omitRelations_(json);
  } else {
    return json;
  }
};

/**
 * Name of the database table of this model.
 *
 * @note Subclasses must set this.
 *
 * @type {String}
 */
SqlModel.tableName = null;

/**
 * The bound database.
 *
 * A knex.js database connection.
 *
 * @type {knex}
 */
SqlModel.db = null;

/**
 * Shortcut to HasOneSqlModelRelation constructor.
 *
 * @type {HasOneSqlModelRelation}
 */
SqlModel.HasOneRelation = HasOneSqlModelRelation;

/**
 * Shortcut to HasManySqlModelRelation constructor.
 *
 * @type {HasManySqlModelRelation}
 */
SqlModel.HasManyRelation = HasManySqlModelRelation;

/**
 * Shortcut to ManyToManySqlModelRelation constructor.
 *
 * @type {ManyToManySqlModelRelation}
 */
SqlModel.ManyToManyRelation = ManyToManySqlModelRelation;

/**
 * Shortcut to SqlModelQueryBuilder.EagerType
 *
 * @enum {Number}
 */
SqlModel.EagerType = SqlModelQueryBuilder.EagerType;

/**
 * Relations to other `SqlModel`s.
 *
 * Can either be a hash of `<relation name, SqlModelRelationMapping>` pairs or a function
 * that returns such a hash.
 *
 * Examples:
 *
 * ```js
 * // HasManyRelation to Person with default join column.
 * Person.relationMappings = {
 *   children: {
 *     modelClass: Person,
 *     relation: SqlModel.HasManyRelation
 *   }
 * }
 *
 * // HasOneRelation to Person with custom join column.
 * Person.relationMappings = {
 *   father: {
 *     modelClass: Person,
 *     relation: SqlModel.HasOneRelation,
 *     joinColumn: 'someColumnName'
 *   }
 * }
 *
 * // HasManyRelation to Person with custom join column.
 * Person.relationMappings = {
 *   children: {
 *     modelClass: Person,
 *     relation: SqlModel.HasManyRelation,
 *     joinColumn: 'someColumnName'
 *   }
 * }
 *
 * // ManyToManyRelation to Person with custom join table.
 * Person.relationMappings = {
 *   children: {
 *     modelClass: Person,
 *     relation: SqlModel.ManyToManyRelation,
 *     join: {
 *       table: 'someCustomTable',
 *       relatedIdColumn: 'customColumn1',
 *       ownerIdColumn: 'customColumn2'
 *     }
 *   }
 * }
 *
 * // String modelClass. Can be used to prevent require loops.
 * Person.relationMappings = {
 *   children: {
 *     modelClass: __dirname + '/some/directory/Person',
 *     relation: SqlModel.HasManyRelation
 *   }
 * }
 *
 * // Another way to prevent require loops is to make relationMappings a function.
 * Person.relationMappings = function () {
 *   return {
 *     father: {
 *       modelClass: require('path/to/models/Person'),
 *       relation: SqlModel.HasOneRelation
 *     }
 *   };
 * }
 *
 * // An additional query can be added to relation mappings.
 * Person.relationMappings = {
 *   teenageChildren: {
 *     modelClass: Person,
 *     relation: SqlModel.HasManyRelation,
 *     query: function (queryBuilder) {
 *       queryBuilder.whereBetween('age', [13, 19]);
 *     }
 *   }
 * }
 *
 * // The additional query can also be an object.
 * Person.relationMappings = {
 *   daughters: {
 *     modelClass: Person,
 *     relation: SqlModel.HasManyRelation,
 *     query: {
 *       gender: 'female'
 *     }
 *   }
 * }
 * ```
 *
 * @type {Object|function:Object|Object.<String, SqlModelRelationMapping>|function:Object.<String, SqlModelRelationMapping>}
 * @see SqlModelRelationMapping
 */
SqlModel.relationMappings = {};

/**
 * Names of the json attributes that should be saved to database as strings.
 *
 * Attributes listed here are automatically converted to a string in `$formatDatabaseJson` and back
 * to JSON object in `$parseDatabaseJson`.
 *
 * @type {Array.<String>}
 */
SqlModel.jsonAttributes = [];

/**
 * If this is true row identifiers are UUIDs generated by SqlModel#generateId() method.
 *
 * Otherwise it is assumed that the database handles the identifiers.
 *
 * @type {Boolean}
 */
SqlModel.uuidRowIdentifiers = false;

/**
 * @private
 * @type {Object.<String, SqlModelRelation>}
 */
SqlModel.relations_ = null;

/**
 * @private
 * @type {Array.<String>}
 */
SqlModel.omitAttributes_ = null;

/**
 * @private
 * @type {Array.<String>}
 */
SqlModel.pickAttributes_ = null;

/**
 * Binds the database and returns a bound constructor.
 *
 * Creates a new subclass of this class and sets its `db`. This also binds all related models
 * to the same database.
 *
 * @param {object} db
 * @returns {function}
 */
SqlModel.bindDb = function (db) {
  // Create cache for models in the database object if it doesn't exist.
  if (!db.boundModelsCache_) {
    db.boundModelsCache_ = [];
  }

  // First search the cache.
  for (var i = 0, l = db.boundModelsCache_.length; i < l; ++i) {
    if (db.boundModelsCache_[i].super_ === this) {
      return db.boundModelsCache_[i];
    }
  }

  return this.doBindDb_(db);
};

/**
 * Creates a new knex.js query builder object.
 *
 * @param {String=} tableName
 *    If not given, this class's table is used.
 *
 * @return {QueryBuilder}
 */
SqlModel.dbQuery = function (tableName) {
  if (!this.db) {
    throw new Error('No database has been bound');
  }
  return this.db.table(tableName || this.tableName);
};

/**
 * Creates a new knex.js query formatter object.
 *
 * @return {Formatter}
 */
SqlModel.dbFormatter = function () {
  if (!this.db) {
    throw new Error('No database has been bound');
  }
  // The formatter is not a part of the documented public API of knex.
  // This check is here to blow up when the API changes.
  if (!this.db.client || !_.isFunction(this.db.client.Formatter)) {
    throw new Error('knex.js API no longer has the Formatter constructor.');
  }
  return new this.db.client.Formatter();
};

/**
 * Starts a knex.js transaction.
 *
 * The callback receives the transaction object. Attach operations to the transaction using
 * the `.transacting(transaction)` method. After you are done with the transaction just return
 * a promise from the callback. If the promise is resolved the transaction is committed. If the
 * promise is rejected the transaction is rolled back.
 *
 * ```js
 * SomeSqlModel.$transaction(function (transaction) {
 *   return SomeSqlModel
 *     .insert({name: 'Jennifer'})
 *     .transacting(transaction);
 * });
 * ```
 *
 * @param {function (Transaction)} callback
 *    The function inside which the transaction is alive.
 *
 * @param {QueryBuilder=} db
 *    Optional knex.js database connection. `this.db` is used by default.
 *
 * @return {Promise}
 */
SqlModel.transaction = function (callback, db) {
  db = db || this.db;
  return db.transaction(function (trx) {
    try {
      Promise.resolve(callback(trx))
        .then(trx.commit)
        .catch(trx.rollback);
    } catch (err) {
      trx.rollback(err);
    }
  });
};

/**
 * Inserts a model to the database.
 *
 * @param {SqlModel|Object} modelOrJson
 *    An SqlModel instance or a JSON object from which it can be created.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.insert = function (modelOrJson) {
  var ModelClass = this;
  var model = ModelClass.ensureModel(modelOrJson);
  delete model.id;

  if (ModelClass.uuidRowIdentifiers) {
    model.id = ModelClass.generateId();
  }

  return SqlModelQueryBuilder
    .forClass(this)
    .insert(model.$toDatabaseJson())
    .returning('id')
    .runAfterQueryExecution(function (id) {
      if (!ModelClass.uuidRowIdentifiers) {
        model.id = id[0];
      }
      return [model];
    })
    .runAfterExecution(function (models) {
      return models[0];
    });
};

/**
 * Reads models from the database.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.find = function () {
  return SqlModelQueryBuilder.forClass(this);
};

/**
 * Reads models from the database using a WHERE IN clause.
 *
 * This is equal to `SomeModel.find().whereIn(column, values)` except that this method has no limits
 * for the size of the values array. The query is split into multiple queries if the size becomes
 * too large for the database to handle.
 *
 * @note The fact that the query may be split into multiple queries causes that some of the methods
 *    of `SqlModelQueryBuilder` will not work as expected. For example mean, avg, count etc. will
 *    give undesired results.
 *
 * @param {String} column
 *    Name of the column for the where in clause:
 *    `SELECT * FROM "SomeModel" WHERE column IN values`
 *
 * @param {Array} values
 *    An array of values to which the column parameter is compared:
 *    `SELECT * FROM "SomeModel" WHERE column IN values`
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.findWhereIn = function (column, values) {
  var ModelClass = this;

  // To prevent the `where in` clause from becoming too large, split the query
  // into chunks of 500 items.
  var valueChunks = arrayUtils.chunked(values, 500);

  return SqlModelQueryBuilder.all(_.map(valueChunks, function (chunk) {
    return SqlModelQueryBuilder
      .forClass(ModelClass)
      .whereIn(column, chunk);
  }));
};

/**
 * Finds models that are related to `models` through `relationName` and saves them to the models.
 *
 * @param {Array.<SqlModel>} models
 *    Models for which to fetch the relation.
 *
 * @param {String} relationName
 *    Name of the relation.
 *
 * @see relationMappings
 * @return {SqlModelQueryBuilder}
 */
SqlModel.findRelated = function (models, relationName) {
  if (models.length === 1) {
    return this
      .getRelation(relationName)
      .find(models[0])
      .runAfterExecution(function (result) {
        return [result];
      });
  } else {
    return this
      .getRelation(relationName)
      .findForMany(models);
  }
};

/**
 * Alias for find that only selects the first result.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.findOne = function () {
  return this.find().runAfterExecution(_.first);
};

/**
 * Alias for `$findOne().where({id: id})`
 *
 * @param {String|Number} id
 * @return {SqlModelQueryBuilder}
 */
SqlModel.findById = function (id) {
  return this.findOne().where('id', id);
};

/**
 * Updates a model in the database.
 *
 * The row to update is selected using `modelOrJson.id` if it is defined. Otherwise
 * a WHERE clause must be explicitly added to the query using one of the `where*`
 * methods of `SqlModelQueryBuilder`.
 *
 * @param {SqlModel|Object} modelOrJson
 *    An SqlModel instance or a JSON object from which it can be created.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.update = function (modelOrJson) {
  var model = this.ensureModel(modelOrJson);
  var json = model.$toDatabaseJson(true);
  return SqlModelQueryBuilder
    .forClass(this)
    .update(json)
    .runFunction(function (query) {
      if (model.id) {
        query.where('id', model.id);
      }
    })
    .runAfterQueryExecution(function (result) {
      if (result === 0) {
        // If no rows were updated, return an empty array.
        return [];
      } else {
        return [model];
      }
    })
    .runAfterExecution(function (models) {
      if (models.length === 0) {
        // If models is an empty array no rows were updated.
        return null;
      } else {
        return models[0];
      }
    });
};

/**
 * Deletes objects from the database.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel.del = function () {
  return SqlModelQueryBuilder.forClass(this).del();
};

/**
 * Alias for `$del`.
 *
 * @return {SqlModelQueryBuilder}
 */
SqlModel['delete'] = function () {
  return this.del();
};

/**
 * Alias for `this.del().where({id: id})`
 *
 * @param {String|Number} id
 * @return {SqlModelQueryBuilder}
 */
SqlModel.deleteById = function (id) {
  return this.del().where('id', id);
};

/**
 * Returns relation mappings.
 *
 * modelClass property of each mapping is converted to a SqlModel subclass using require
 * (if modelClass is a string).
 *
 * @return {Object.<String, SqlModelRelationMapping>}
 */
SqlModel.getRelationMappings = function () {
  var ModelClass = this;

  return _.mapValues(_.result(this, 'relationMappings'), function (mapping, relationName) {
    var errorPrefix = ModelClass.name + ".relationMappings." + relationName;

    if (!mapping.modelClass) {
      throw new Error(errorPrefix + '.modelClass is not defined');
    }
    if (_.isString(mapping.modelClass)) {
      mapping.modelClass = require(mapping.modelClass);
    }
    if (!classUtils.isSubclassOf(mapping.modelClass, SqlModel)) {
      throw new Error(errorPrefix + '.modelClass is not a subclass of SqlModel');
    }

    if (!mapping.relation) {
      throw new Error(errorPrefix + '.relation is not defined');
    }
    if (!classUtils.isSubclassOf(mapping.relation, SqlModelRelation)) {
      throw new Error(errorPrefix + '.relation is not a subclass of SqlModelRelation');
    }

    return mapping;
  });
};

/**
 * Generates a unique id for an SqlModel.
 *
 * @return {String}
 */
SqlModel.generateId = function () {
  return uuid.v1();
};

/**
 * @return {Object.<String, SqlModelRelation>}
 */
SqlModel.getRelations = function () {
  if (!this.relations_) {
    this.relations_ = this.createRelations_();
  }
  return this.relations_;
};

/**
 * @return {SqlModelRelation}
 */
SqlModel.getRelation = function (name) {
  var relation = this.getRelations()[name];
  if (!relation) {
    throw new Error("model class '" + this.tableName + "' doesn't have relation '" + name + "'");
  }
  return relation;
};

/**
 * @param {SqlModel|Object} modelOrJson
 * @returns {SqlModel}
 */
SqlModel.ensureModel = function (modelOrJson) {
  if (modelOrJson instanceof this) {
    return modelOrJson;
  } else {
    return this.fromJson(modelOrJson);
  }
};

/**
 * @private
 * @return {Object.<String, SqlModelRelation>}
 */
SqlModel.createRelations_ = function () {
  return _.mapValues(this.getRelationMappings(), function (mapping, relationName) {
    return new mapping.relation(relationName, mapping, this);
  }, this);
};

/**
 * @private
 */
SqlModel.omitNonColumns_ = function (json) {
  if (this.schema) {
    if (!this.pickAttributes_) {
      this.pickAttributes_ = _.keys(this.schema.properties);
    }
    return _.pick(json, this.pickAttributes_);
  } else {
    return this.omitRelations_(json);
  }
};

/**
 * @private
 */
SqlModel.omitRelations_ = function (json) {
  if (!this.omitAttributes_) {
    this.omitAttributes_ = _.keys(this.getRelations());
  }
  if (this.omitAttributes_.length) {
    return _.omit(json, this.omitAttributes_);
  }
  return json;
};

/**
 * @private
 */
SqlModel.doBindDb_ = function (db) {
  if (this.db) {
    throw new Error(this.name + ' is already bound');
  }

  var ThisClass = this;
  var BoundModelClass = function () { ThisClass.apply(this, arguments); };
  classUtils.inherits(BoundModelClass, ThisClass);

  // Need to add to cache at this point because there may be recursive relations.
  db.boundModelsCache_.push(BoundModelClass);

  BoundModelClass.db = db;
  BoundModelClass.relations_ = _.mapValues(this.getRelations(), function (relation) {
    relation = relation.clone();
    relation.ownerModelClass = BoundModelClass;
    relation.relatedModelClass = relation.relatedModelClass.bindDb(db);
    return relation;
  });

  return BoundModelClass;
};

module.exports = SqlModel;
