"use strict";

var _ = require('lodash')
  , classUtils = require('../../../class-utils')
  , SqlModelQueryBuilder = require('../SqlModelQueryBuilder')
  , SqlModelRelation = require('./SqlModelRelation');

/**
 * @constructor
 * @extends SqlModelRelation
 */
function HasManySqlModelRelation() {
  SqlModelRelation.apply(this, arguments);
}

classUtils.inherits(HasManySqlModelRelation, SqlModelRelation);

/**
 * @override
 */
HasManySqlModelRelation.prototype.find = function (model) {
  var self = this;
  return this.relatedModelClass
    .find()
    .where(self.ownerJoinColumn, model.id)
    .runFunction(this.additionalQuery)
    .runAfterExecution(function (relationModels) {
      model[self.name] = relationModels;
      return relationModels;
    });
};

/**
 * @override
 */
HasManySqlModelRelation.prototype.findForMany = function (models) {
  var self = this
    , modelsById = {}
    , joinIds;

  if (_.isEmpty(models)) {
    return SqlModelQueryBuilder.returning([]);
  }

  _.each(models, function (model) {
    var id = model.id;
    model[self.name] = [];
    if (!modelsById[id]) { modelsById[id] = []; }
    modelsById[id].push(model);
  });

  joinIds = _.keys(modelsById);
  return this.relatedModelClass
    .findWhereIn(self.ownerJoinColumn, joinIds)
    .runFunction(this.additionalQuery)
    .runAfterExecution(function (relationModels) {
      return _.map(relationModels, function (relationModel) {
        var joinId = relationModel[self.ownerJoinColumn];
        var models = modelsById[joinId];
        for (var i = 0, l = models.length; i < l; ++i) {
          models[i][self.name].push(relationModel);
        }
        return relationModel;
      });
    });
};

/**
 * @override
 */
HasManySqlModelRelation.prototype.insert = function (model, modelToInsert) {
  var self = this;
  modelToInsert[this.ownerJoinColumn] = model.id;
  return this.relatedModelClass
    .insert(modelToInsert)
    .runAfterExecution(function (insertedModel) {
      model[self.name] = model[self.name] || [];
      model[self.name].push(insertedModel);
      return insertedModel;
    });
};

/**
 * @override
 */
HasManySqlModelRelation.prototype.del = function (model, idOfModelToDelete) {
  var self = this;
  return this.relatedModelClass
    .deleteById(idOfModelToDelete)
    .where(this.ownerJoinColumn, model.id)
    .runAfterExecution(function (input) {
      _.remove(model[self.name], {id: idOfModelToDelete});
      return input;
    });
};

module.exports = HasManySqlModelRelation;
