"use strict";

var tv4 = require('tv4')
  , formats = require('tv4-formats')
  , jsonUtils = require('../utils/json-utils')
  , ValidationError = require('../errors/ValidationError');

// Add validation formats, so that for example the following schema validation works:
// createTime: {type: 'string', format: 'date-time'}
tv4.addFormat(formats);

/**
 * Base class for models.
 *
 * This class is in no way tied to any ORM or database. `Model` simply provides a mechanism for
 * automatic JSON validation and a way to attach functionality to plain javascript objects.
 *
 * Use `Model.from*Json` methods to create models from JSON objects. You should never need to call
 * the constructor explicitly. The `Model.from*Json` methods copy the json properties to the created
 * model like this:
 *
 * ```js
 * var model = Model.fromJson({foo: 'bar', id: 10});
 * console.log(model.foo); // --> bar
 * console.log(model.id); // --> 10
 * ```
 *
 * Properties that are prefixed with '$' are excluded from all JSON representations.
 *
 * ```js
 * var model = Model.fromJson({foo: 'bar', id: 10});
 * model.$spam = 100;
 * console.log(model); // --> {foo: 'bar', id: 10, $spam: 100}
 * console.log(model.$toJson()); // --> {foo: 'bar', id: 10}
 * ```
 *
 * Set the static property `schema` to enable validation. Validation is performed when models
 * are created using the `fromJson` method or when the model is updated using the `$setJson`
 * method.
 *
 * @constructor
 */
function Model() {
  // Nothing to do here.
}

/**
 * This is called before validation.
 *
 * Here you can dynamically edit the schema if needed.
 *
 * @param {Object} schema
 *    A deep clone of this.constructor.schema
 *
 * @param {Object} json
 *    The JSON object to be validated.
 *
 * @return {Object}
 *    The (possibly) modified schema.
 */
Model.prototype.$beforeValidate = function (schema, json) {
  return schema;
};

/**
 * Validates the given JSON object.
 *
 * Calls `$beforeValidation` and `$afterValidation` methods. This method is called
 * automatically from `fromJson` and `$setJson` methods. This method can also be
 * called explicitly when needed.
 *
 * @throws ValidationError
 *    If validation fails.
 *
 * @param {Object=} json
 *    If not given ==> this.
 *
 * @returns {Object} The input json
 */
Model.prototype.$validate = function (json) {
  var schema = this.constructor.schema
    , validationError
    , report;

  json = json || this;

  if (!schema) {
    return json;
  }

  // No need to call $beforeValidate (and clone the schema) if
  // $beforeValidate has not been overwritten.
  if (this.$beforeValidate !== Model.prototype.$beforeValidate) {
    schema = jsonUtils.deepClone(schema);
    schema = this.$beforeValidate(schema, json);
  }

  report = tv4.validateMultiple(json, schema);
  validationError = this.$parseValidationError_(report);

  if (validationError) {
    throw validationError;
  }

  this.$afterValidate();
  return json;
};

/**
 * This is called after successful validation.
 */
Model.prototype.$afterValidate = function () {
  // Do nothing by default.
};

/**
 * This is called when a `Model` is created from a database JSON object.
 *
 * Converts the JSON object from the database format to the internal format.
 *
 * @note If you override this remember to call the super class's implementation.
 *
 * @param {Object} json
 *    The JSON object in database format.
 *
 * @return {Object}
 *    The JSON object in internal format.
 */
Model.prototype.$parseDatabaseJson = function (json) {
  return json;
};

/**
 * This is called when a `Model` is converted to database format.
 *
 * Converts the JSON object from the internal format to the database format.
 *
 * @note If you override this remember to call the super class's implementation.
 *
 * @param {Object} json
 *    The JSON object in internal format.
 *
 * @return {Object}
 *    The JSON object in database format.
 */
Model.prototype.$formatDatabaseJson = function (json) {
  return json;
};

/**
 * This is called when a `Model` is created from a JSON object.
 *
 * Converts the JSON object to the internal format.
 *
 * @note If you override this remember to call the super class's implementation.
 *
 * @param {Object} json
 *    The JSON object in external format.
 *
 * @return {Object}
 *    The JSON object in internal format.
 */
Model.prototype.$parseJson = function (json) {
  return json;
};

/**
 * This is called when a `Model` is converted to JSON.
 *
 * @note Remember to call the super class's implementation.
 *
 * @param {Object} json
 *    The JSON object in internal format
 *
 * @return {Object}
 *    The JSON object in external format.
 */
Model.prototype.$formatJson = function (json) {
  return json;
};

/**
 * Exports this model as a database JSON object.
 *
 * Calls `$formatDatabaseJson()`.
 *
 * @return {Object}
 *    This model as a JSON object in database format.
 */
Model.prototype.$toDatabaseJson = function () {
  return this.$formatDatabaseJson(this.$toJson_(true));
};

/**
 * Exports this model as a JSON object.
 *
 * Calls `$formatJson()`.
 *
 * @return {Object}
 *    This model as a JSON object.
 */
Model.prototype.$toJson = function () {
  return this.$formatJson(this.$toJson_(false));
};

/**
 * Alias for `this.$toJson()`.
 *
 * For JSON.serialize compatibility.
 */
Model.prototype.toJSON = function () {
  return this.$toJson();
};

/**
 * Sets the values from a JSON object.
 *
 * Validates the JSON before setting values. Calls `this.$parseJson()`.
 *
 * @param {Object} json
 *    The JSON object to set.
 *
 * @throws ValidationError
 *    If validation fails.
 */
Model.prototype.$setJson = function (json) {
  json = this.$validate(this.$parseJson(json || {}));
  for (var key in json) {
    if (json.hasOwnProperty(key)) {
      this[key] = json[key];
    }
  }
};

/**
 * Sets the values from a JSON object in database format.
 *
 * Calls `this.$parseDatabaseJson()`.
 *
 * @param {Object} json
 *    The JSON object in database format.
 */
Model.prototype.$setDatabaseJson = function (json) {
  json = this.$parseDatabaseJson(json || {});
  for (var key in json) {
    if (json.hasOwnProperty(key)) {
      this[key] = json[key];
    }
  }
};

/**
 * Returns a deep copy of this model.
 *
 * @return {Model}
 */
Model.prototype.$clone = function () {
  var copy = new this.constructor(), key, value, arr, i, l;

  for (key in this) {
    if (key.charAt(0) === '$' || !this.hasOwnProperty(key)) {
      continue;
    }
    value = this[key];
    if (Array.isArray(value)) {
      arr = [];
      for (i = 0, l = value.length; i < l; ++i) {
        if (value[i] instanceof Model) {
          arr.push(value[i].$clone());
        } else {
          arr.push(jsonUtils.deepClone(value[i]));
        }
      }
      copy[key] = arr;
    } else if ('object' === typeof value) {
      if (value instanceof Model) {
        copy[key] = value.$clone();
      } else {
        copy[key] = jsonUtils.deepClone(value);
      }
    } else {
      copy[key] = value;
    }
  }

  return copy;
};

/**
 * The schema against which the JSON is validated.
 *
 * The schema can be dynamically modified in the `$beforeValidate` method.
 *
 * Must follow http://json-schema.org specification. If null no validation is done.
 *
 * @see $beforeValidate()
 * @see $validate()
 * @see $afterValidate()
 * @type {Object}
 */
Model.schema = null;

/**
 * Creates a model instance from a JSON object.
 *
 * The JSON schema is checked against `schema` and an exception is thrown on failure.
 *
 * @param {Object=} json
 *    The JSON from which to create the model.
 *
 * @throws {ValidationError}
 *    If validation fails.
 */
Model.fromJson = function (json) {
  var model = new this();
  model.$setJson(this.mergeWithDefaults_(json));
  return model;
};

/**
 * Creates a model instance from a JSON object in database format.
 *
 * @param {Object=} json
 *    The JSON from which to create the model.
 */
Model.fromDatabaseJson = function(json) {
  var model = new this();
  model.$setDatabaseJson(json || {});
  return model;
};

/**
 * @private
 */
Model.prototype.$toJson_ = function (createDbJson) {
  var json = {}, key, value, arr, i, l;

  for (key in this) {
    if (key.charAt(0) === '$' || !this.hasOwnProperty(key)) {
      continue;
    }
    value = this[key];
    if (Array.isArray(value)) {
      arr = [];
      for (i = 0, l = value.length; i < l; ++i) {
        if (value[i] instanceof Model) {
          arr.push(value[i].$toJson_(createDbJson));
        } else {
          arr.push(jsonUtils.deepClone(value[i]));
        }
      }
      json[key] = arr;
    } else if ('object' === typeof value) {
      if (value instanceof Model) {
        json[key] = value.$toJson_(createDbJson);
      } else {
        json[key] = jsonUtils.deepClone(value);
      }
    } else {
      json[key] = value;
    }
  }

  if (createDbJson) {
    return this.$formatDatabaseJson(json);
  } else {
    return this.$formatJson(json);
  }
};

/**
 * @private
 */
Model.prototype.$parseValidationError_ = function (report) {
  var errorHash = {}, index = 0, error, key, i;

  if (report.errors.length === 0) {
    return null;
  }

  for (i = 0; i < report.errors.length; ++i) {
    error = report.errors[i];
    key = error.dataPath.split('/').slice(1).join('.');

    // Hack: The dataPath is empty for failed 'required' validations. We parse
    // the property name from the error message.
    if (!key && error.message.substring(0, 26) === 'Missing required property:') {
      key = error.message.split(':')[1].trim();
    }

    // If the validation failed because of extra properties, the key is an empty string. We
    // still want a unique error in the hash for each failure.
    if (!key) {
      key = (index++).toString();
    }

    errorHash[key] = error.message;
  }

  return new ValidationError(errorHash);
};

/**
 * @private
 */
Model.mergeWithDefaults_ = function (json) {
  var schema = this.schema, merged = null, prop, key;

  if (!schema) {
    return json;
  }

  for (key in schema.properties) {
    if (!(key in json)) {
      prop = schema.properties[key];
      if ('default' in prop) {
        if (merged === null) {
          merged = jsonUtils.deepClone(json);
        }
        if ('object' === typeof prop.default) {
          merged[key] = jsonUtils.deepClone(prop.default);
        } else {
          merged[key] = prop.default;
        }
      }
    }
  }

  if (merged === null) {
    return json;
  } else {
    return merged;
  }
};

module.exports = Model;
