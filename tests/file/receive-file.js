var _ = require('lodash')
  , os = require('os')
  , fs = require('fs')
  , expect = require('expect.js')
  , express = require('express')
  , Promise = require('bluebird')
  , bodyParser = require('body-parser')
  , request = require('../../http/request')
  , receiveFile = require('../../file/receive-file')
  , sentFile = os.tmpdir() + 'sent'
  , receivedFile = os.tmpdir() + 'received';

Promise.longStackTraces();

// Create a file filled with random characters.
function createFile(filePath, size) {
  var buf = new Buffer(size);
  var str = 'abcdefghijklmnopqrstuvxyz';
  for (var i = 0; i < size; ++i) {
    buf[i] = str.charAt(Math.round(Math.random() * (str.length - 1)));
  }
  fs.writeFileSync(filePath, buf);
}

// Compare contents of two files.
function compareFiles(filePath1, filePath2) {
  var file1 = fs.readFileSync(filePath1);
  var file2 = fs.readFileSync(filePath2);
  return file1.toString() === file2.toString();
}

describe('receive-file', function() {
  var app = null;
  var sizeLimit;

  before(function (done) {
    app = express();
    app.use(bodyParser.json());

    app.post('/test', function(req, res) {
      receiveFile(req)
        .sizeLimit(sizeLimit)
        .path(receivedFile)
        .then(function(result) {
          res.status(200).json(result);
        })
        .catch(function(error) {
          res.status(400).json(error);
        });
    });

    app.server = require('http').createServer(app);
    app.server.listen(8088, void 0, void 0, function () {
      done();
    });
  });

  after(function(done) {
    app.server.close(done);
  });

  beforeEach(function() {
    if (fs.existsSync(sentFile)) {
      fs.unlinkSync(sentFile);
    }
    if (fs.existsSync(receivedFile)) {
      fs.unlinkSync(receivedFile);
    }
    sizeLimit = 1024 * 1024 * 10;
  });

  it('should receive the sent file', function() {
    createFile(sentFile, 1024);
    return request
      .post('http://localhost:8088/test')
      .multipartFile('file', sentFile)
      .then(function(res) {
        expect(res.statusCode).to.equal(200);
        expect(fs.existsSync(receivedFile)).to.equal(true);
        expect(compareFiles(sentFile, receivedFile)).to.equal(true);
      });
  });

  it('should receive the sent file and additional fields', function() {
    createFile(sentFile, 1024);
    return request
      .post('http://localhost:8088/test')
      .multipart('someField', 'someValue')
      .multipart('someOtherField', 'someOtherValue')
      .multipartFile('file', sentFile)
      .then(function(res) {
        expect(res.statusCode).to.equal(200);
        expect(fs.existsSync(receivedFile)).to.equal(true);
        expect(compareFiles(sentFile, receivedFile)).to.equal(true);
        expect(res.body.fields).to.eql({someField: 'someValue', someOtherField: 'someOtherValue'});
        expect(res.body.path).to.eql(receivedFile);
      });
  });

  it('should fail if the sent file is too large', function() {
    sizeLimit = 1000;
    createFile(sentFile, 1024);
    return request
      .post('http://localhost:8088/test')
      .multipartFile('file', sentFile)
      .then(function(res) {
        expect(res.statusCode).to.equal(400);
        expect(fs.existsSync(receivedFile)).to.equal(false);
      });
  });

  it('should fail with multiple files', function() {
    createFile(sentFile, 1024);
    return request
      .post('http://localhost:8088/test')
      .multipartFile('file1', sentFile)
      .multipartFile('file2', sentFile)
      .then(function(res) {
        expect(res.statusCode).to.equal(400);
        expect(fs.existsSync(receivedFile)).to.equal(false);
      });
  });

  it('should fail if there is no file (1)', function() {
    createFile(sentFile, 10 * 1024);
    return request
      .post('http://localhost:8088/test')
      .then(function(res) {
        expect(res.statusCode).to.equal(400);
        expect(fs.existsSync(receivedFile)).to.equal(false);
      });
  });

  it('should fail if there is no file (2)', function() {
    createFile(sentFile, 10 * 1024);
    return request
      .post('http://localhost:8088/test')
      .multipart('someField', 'someValue')
      .then(function(res) {
        expect(res.statusCode).to.equal(400);
        expect(fs.existsSync(receivedFile)).to.equal(false);
      });
  });
});
