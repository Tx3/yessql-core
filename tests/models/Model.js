var _ = require('lodash')
  , expect = require('expect.js');

var Model = require('../../models/Model')
  , classUtils = require('../../class-utils')
  , ValidationError = require('../../errors/ValidationError');

describe('Model tests', function () {
  var nestedJson;

  beforeEach(function () {
    nestedJson = {
      a: 1,
      b: {
        d: '2',
        e: [
          {f: '3'},
          {f: '4'}
        ]
      }
    };
  });

  var cloneJson = function (json) {
    return JSON.parse(JSON.stringify(json));
  };

  it('should create an empty model', function () {
    var model1 = Model.fromDatabaseJson();
    var model2 = Model.fromJson();

    expect(model1.$toDatabaseJson()).to.eql({});
    expect(model1.$toJson()).to.eql({});

    expect(model2.$toDatabaseJson()).to.eql({});
    expect(model2.$toJson()).to.eql({});
  });

  it('by default all json representations should be equal', function () {
    var json = cloneJson(nestedJson);
    var model = Model.fromDatabaseJson(json);

    expect(model.$toDatabaseJson()).to.eql(json);
    expect(model.$toJson()).to.eql(json);
    expect(model.toJSON()).to.eql(json);

    json = cloneJson(nestedJson);
    model = Model.fromJson(json);

    expect(model.$toDatabaseJson()).to.eql(json);
    expect(model.$toJson()).to.eql(json);
    expect(model.toJSON()).to.eql(json);
  });

  it('should not add properties starting with $ to json', function () {
    var json = cloneJson(nestedJson);
    var model = Model.fromJson(json);

    model.$test = 100;
    model.$$test2 = 'test';

    expect(model.$toDatabaseJson()).to.eql(json);
    expect(model.$toJson()).to.eql(json);
    expect(model.toJSON()).to.eql(json);
  });

  it('$clone should create a deep copy', function () {
    var json = cloneJson(nestedJson);

    var model = Model.fromJson(json);
    model.subModel = Model.fromJson(json);
    var clone = model.$clone();

    expect(model).to.eql(clone);
    // Strict equality of the objects should be false because they are copies.
    expect(model.subModel).to.not.equal(clone.subModel);
    expect(model.subModel.b).to.not.equal(clone.subModel.b);
    expect(model.b).to.not.equal(clone.b);
    expect(model.b.e).to.not.equal(clone.b.e);
    expect(model.b.e[0]).to.not.equal(clone.b.e[0]);
    expect(model.b.e[1]).to.not.equal(clone.b.e[1]);
  });

  it('should run $parseDatabaseJson and $formatDatabaseJson', function () {
    // Model that adds a property newProp when coming from database format.
    function Sub() {
      Model.call(this);
    }

    classUtils.inherits(Sub, Model);

    Sub.prototype.$parseDatabaseJson = function (json) {
      json = Model.prototype.$parseDatabaseJson.call(this, json);
      json.newProp = true;
      return json;
    };

    Sub.prototype.$formatDatabaseJson = function (json) {
      json = Model.prototype.$formatDatabaseJson.call(this, json);
      return _.omit(json, 'newProp');
    };

    var model1 = Sub.fromDatabaseJson(cloneJson(nestedJson));
    var model2 = Sub.fromJson(cloneJson(nestedJson));

    expect(model1.newProp).to.eql(true);
    expect(model2).to.not.have.property('newProp');
    expect(model1.$toDatabaseJson()).to.eql(nestedJson);
    expect(model2.$toDatabaseJson()).to.eql(nestedJson);
  });

  it('should run $parseJson and $formatJson', function () {
    // Model that adds a property newProp when coming from request/response format.
    function Sub() {
      Model.call(this);
    }

    classUtils.inherits(Sub, Model);

    Sub.prototype.$parseJson = function(json) {
      json = Model.prototype.$parseJson.call(this, json);
      json.newProp = true;
      return json;
    };

    Sub.prototype.$formatJson = function(json) {
      json = Model.prototype.$formatJson.call(this, json);
      delete json.newProp;
      return json;
    };

    var model1 = Sub.fromJson(cloneJson(nestedJson));
    var model2 = Sub.fromDatabaseJson(cloneJson(nestedJson));

    expect(model1.newProp).to.eql(true);
    expect(model2).to.not.have.property('newProp');
    expect(model1.$toJson()).to.eql(nestedJson);
    expect(model2.$toJson()).to.eql(nestedJson);
  });

  it('should pass validation', function () {
    var json = cloneJson(nestedJson);

    function Sub() {
      Model.call(this);
    }

    classUtils.inherits(Sub, Model);

    Sub.schema = {
      type: 'object',
      additionalProperties: false,
      required: ['a', 'b'],

      properties: {
        a:  {type: 'number'},
        b:  {type: 'object'}
      }
    };

    expect(function() { Sub.fromJson(json); }).to.not.throwException();
  });

  it('failed validation should throw ValidationError', function () {
    var json = cloneJson(nestedJson);

    function Sub() {
      Model.call(this);
    }

    classUtils.inherits(Sub, Model);

    Sub.schema = {
      type: 'object',
      additionalProperties: false,
      required: ['a', 'b'],

      properties: {
        a:  {type: 'string'},
        b:  {type: 'object'}
      }
    };

    expect(function() { Sub.fromJson(json); }).to.throwException(function (error) {
      expect(error).to.be.a(ValidationError);
      expect(error).to.have.property('data');
      expect(error.data).to.have.property('a');
    });
  });

  it('should throw ValidationError on failed date-time format validation', function () {
    function Sub() {
      Model.call(this);
    }

    classUtils.inherits(Sub, Model);

    Sub.schema = {
      type: 'object',

      properties: {
        a: {type: 'string', format: 'date'},
        b: {type: 'string', format: 'date'},
        c: {type: 'string', format: 'date'},
        d: {type: 'string', format: 'date-time'},
        e: {type: 'string', format: 'date-time'},
        f: {type: 'string', format: 'date-time'},
        g: {type: 'string', format: 'date-time'},
        h: {type: 'string', format: 'date'},
        i: {type: 'string', format: 'date'},
        j: {type: 'string', format: 'date-time'},
        k: {type: 'string', format: 'date-time'}
      }
    };

    expect(function () {
      Sub.fromJson({
        a:'invalid-date',
        b:'2014-02-29',
        c:'2014-13-01',
        d:'invalid-date-time',
        e:'2014-02-29T10:55:00+03:00',
        f:'2014-02-28T10:61:00+03:00',
        g:'2014-13-29T10:55:00+03:00',
        h:'2014-10-21',
        i:'2016-02-29',
        j:'2014-10-21T10:55:00+03:00',
        k:'2016-10-29T10:55:00+03:00'
      });
    }).to.throwException(function (error) {
        expect(error).to.be.a(ValidationError);
        expect(error).to.have.property('data');
        expect(error.data).to.have.property('a');
        expect(error.data).to.have.property('b');
        expect(error.data).to.have.property('c');
        expect(error.data).to.have.property('d');
        expect(error.data).to.have.property('e');
        expect(error.data).to.have.property('f');
        expect(error.data).to.have.property('g');
        expect(error.data).not.to.have.property('h');
        expect(error.data).not.to.have.property('i');
        expect(error.data).not.to.have.property('j');
        expect(error.data).not.to.have.property('k');
      });
  });

  it('should add model properties to JSON', function () {
    var json = cloneJson(nestedJson);

    function Sub() {
      Model.call(this);
    }

    classUtils.inherits(Sub, Model);

    var model1 = Sub.fromDatabaseJson(json);
    model1.test = Sub.fromDatabaseJson(json);

    expect(_.omit(model1.$toDatabaseJson(), 'test')).to.eql(json);
    expect(model1.$toDatabaseJson().test).to.eql(json);

    expect(_.omit(model1.$toJson(), 'test')).to.eql(json);
    expect(model1.$toJson().test).to.eql(json);
  });

  it('should apply defaults', function () {
    function Sub() {
      Model.call(this);
    }

    classUtils.inherits(Sub, Model);

    Sub.schema = {
      type: 'object',

      properties: {
        a: {type: 'number', default: 10},
        b: {type: 'object', default: {b: [{c:50}]}}
      }
    };

    var model1 = Sub.fromJson({c:30, d:40});

    expect(model1.toJSON()).to.eql({a:10, b:{b:[{c:50}]}, c:30, d:40});
    expect(model1.b === Sub.schema.properties.b.default).to.eql(false);
    expect(model1.b.b === Sub.schema.properties.b.default.b).to.eql(false);
    expect(model1.b.b[0] === Sub.schema.properties.b.default.b[0]).to.eql(false);

    var model2 = Sub.fromJson({a:1000, b:{}});
    expect(model2.toJSON()).to.eql({a:1000, b:{}});
  });

});
