var _ = require('lodash')
  , expect = require('expect.js')
  , Promise = require('bluebird');

var SqlModel = require('../../../models/SqlModel')
  , classUtils = require('../../../class-utils');

module.exports = function (shared) {
  var UUID_REGEX = /^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$/;

  describe('basic', function () {
    function Model1() {
      SqlModel.call(this);
    }

    classUtils.inherits(Model1, SqlModel);
    Model1 = Model1.bindDb(shared.db);

    beforeEach(function () {
      return shared.db.schema.createTable('temp', function (table) {
        table.uuid('id').primary();
        table.string('column');
      });
    });

    afterEach(function () {
      return shared.db.schema.dropTableIfExists('temp');
    });

    it('should convert columns listed in jsonAttributes between json and string', function () {
      Model1.tableName = 'model1';
      Model1.jsonAttributes = ['json'];

      var model1 = Model1.fromDatabaseJson({json: '{"a":10,"b":["a","b"]}'});

      expect(model1.$toJson()).to.eql({json: {a: 10, b: ["a", "b"]}});
      expect(model1.$toDatabaseJson()).to.eql({json: '{"a":10,"b":["a","b"]}'});
    });

    it('should use UUIDs as row identifiers if uuidRowIdentifiers is true', function () {
      Model1.tableName = 'temp';
      Model1.jsonAttributes = [];
      Model1.uuidRowIdentifiers = true;

      return Model1.insert(Model1.fromJson({column: 'test'}))
        .then(function (model) {
          expect(model).to.be.a(Model1);
          expect(model.column).to.eql('test');
          expect(model.id).to.match(UUID_REGEX);
          return shared.db('temp');
        })
        .then(function (rows) {
          expect(rows[0].id).to.match(UUID_REGEX);
        });
    });

    it('should leave id creation for database if uuidRowIdentifiers is false', function () {
      Model1.tableName = 'temp';
      Model1.jsonAttributes = [];
      Model1.uuidRowIdentifiers = false;

      return Model1.insert({column: 'test'})
        .then(function (model) {
          expect(model).to.be.a(Model1);
          expect(model.column).to.eql('test');
          expect(model.id).not.to.be.an(Array);
          expect(model.id).to.eql(1);
          return Model1.insert({column: 'test 10'});
        })
        .then(function (model) {
          expect(model).to.be.a(Model1);
          expect(model.column).to.eql('test 10');
          expect(model.id).not.to.be.an(Array);
          expect(model.id).to.eql(2);
        });
    });

    it('should serialize and deserialize relations', function () {
      function Model2() {
        SqlModel.call(this);
      }

      classUtils.inherits(Model2, SqlModel);

      Model2.tableName = 'Model2';
      Model2.relationMappings = {
        hasManyRelation: {
          relation: SqlModel.HasManyRelation,
          modelClass: Model1
        },
        hasOneRelation: {
          relation: SqlModel.HasOneRelation,
          modelClass: Model1
        }
      };

      var json = {
        a: 10,
        hasManyRelation: [
          {b:20},
          {b:30}
        ],
        hasOneRelation: {
          b: 40
        }
      };

      var model = Model2.fromJson(json);
      expect(model.a).to.equal(10);

      expect(model.hasManyRelation[0] instanceof Model1).to.equal(true);
      expect(model.hasManyRelation[0].$toJson()).to.eql({b:20});

      expect(model.hasManyRelation[1] instanceof Model1).to.equal(true);
      expect(model.hasManyRelation[1].$toJson()).to.eql({b:30});

      expect(model.hasOneRelation instanceof Model1).to.equal(true);
      expect(model.hasOneRelation.$toJson()).to.eql({b:40});

      expect(model.$toJson()).to.eql(json);
    });

  });

};
