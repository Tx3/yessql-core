var _ = require('lodash')
  , expect = require('expect.js')
  , Promise = require('bluebird');

module.exports = function (shared) {
  var Model1 = shared.modelClasses.Model1;
  var Model2 = shared.modelClasses.Model2;
  var models = shared.models;

  describe('#insert()', function () {

    it('should insert model1[0] (instance)', function () {
      return insertWithInstanceInsert(models.model1[0]);
    });

    it('should insert model1[0] (static)', function () {
      return insertWithStaticInsert(models.model1[0].$clone());
    });

    it('should insert model1[0] two times in a transaction', function () {
      var model1 = models.model1[0].$clone();
      var model2 = models.model1[0].$clone();

      model1.textProperty1 = 'gleeba gleeba blaa blaa';
      model2.textProperty1 = 'hackfest 2014';

      return Model1.transaction(function (trx) {
        return Model1
          .insert(model1)
          .transacting(trx)
          .then(function () {
            return model2.$insert().transacting(trx);
          });
      }).then(function (result) {
        expect(result.$toJson()).to.eql(model2.$toJson());
        return shared.db(Model1.tableName).whereIn('id', [model1.id, model2.id]);
      }).then(function (rows) {
        expect(rows[0]).to.eql(model1.$toDatabaseJson(true));
        expect(rows[1]).to.eql(model2.$toDatabaseJson(true));
      });
    });

    it('should not insert if transaction fails', function () {
      var model1 = models.model1[0].$clone();
      var model2 = models.model1[0].$clone();

      model1.textProperty1 = 'gleeba gleeba blaa blaa';
      model2.textProperty1 = 'hackfest 2014';

      var transactionFailed = false;
      return Model1.transaction(function (trx) {
        return Model1
          .insert(model1)
          .transacting(trx)
          .then(function () {
            return model2.$insert().transacting(trx);
          })
          .then(function () {
            throw new Error();
          });
      }).catch(function (result) {
        transactionFailed = true;
        expect(result).to.be.an(Error);
        expect(_.isString(model1.id) && _.isString(model2.id)).to.equal(true);
        return shared.db(Model1.tableName).whereIn('id', [model1.id, model2.id]);
      }).then(function (rows) {
        expect(transactionFailed).to.equal(true);
        expect(rows).to.be.an(Array);
        expect(rows).to.have.length(0);
      });
    });

  });


  describe('#insertRelated()', function () {

    it('should insert model2[0] to model1[0].child', function () {
      return insertWithInsertRelated(models.model1[0], 'child', models.model2[0]);
    });

    it('should insert model2[1] to model1[0].customChild', function () {
      return insertWithInsertRelated(models.model1[0], 'customChild', models.model2[1]);
    });


    it('should insert model2[2] to model1[0].children1', function () {
      return insertWithInsertRelated(models.model1[0], 'children1', models.model2[2]);
    });

    it('should insert model2[3] to model1[0].children1', function () {
      return insertWithInsertRelated(models.model1[0], 'children1', models.model2[3]);
    });


    it('should insert model2[4] to model1[0].children2', function () {
      return insertWithInsertRelated(models.model1[0], 'children2', models.model2[4]);
    });

    it('should insert model2[5] to model1[0].children2', function () {
      return insertWithInsertRelated(models.model1[0], 'children2', models.model2[5]);
    });


    it('should insert model3[0] to model2[4].grandChildren1', function () {
      return insertWithInsertRelated(models.model2[4], 'grandChildren1', models.model3[0]);
    });

    it('should insert model3[1] to model2[4].grandChildren2', function () {
      return insertWithInsertRelated(models.model2[4], 'grandChildren2', models.model3[1]);
    });


    it('should insert model3[2] to model2[5].grandChildren1', function () {
      return insertWithInsertRelated(models.model2[5], 'grandChildren1', models.model3[2]);
    });

    it('should insert model3[3] to model2[5].grandChildren2', function () {
      return insertWithInsertRelated(models.model2[5], 'grandChildren2', models.model3[3]);
    });

    it('should insert model3[4] to model2[5].grandChildren2', function () {
      return insertWithInsertRelated(models.model2[5], 'grandChildren2', models.model3[4]);
    });

    it('should insert model3[5] to model2[5].grandChildren2', function () {
      return insertWithInsertRelated(models.model2[5], 'grandChildren2', models.model3[5]);
    });


    it('should have set model1[0].test2Id to model2[0].id', function (done) {
      shared.db(Model1.tableName).where('id', models.model1[0].id).then(function (rows) {
        expect(rows[0].test2Id).to.eql(models.model2[0].id);
        done();
      }).done();
    });

    it('should have set model1[0].customTest2Id to model2[1].id', function (done) {
      shared.db(Model1.tableName).where('id', models.model1[0].id).then(function (rows) {
        expect(rows[0].customTest2Id).to.eql(models.model2[1].id);
        done();
      }).done();
    });

    it('should have set model2[2].test1Id to model1[0].id', function (done) {
      shared.db(Model2.tableName).where('id', models.model2[2].id).then(function (rows) {
        expect(rows[0].test1Id).to.eql(models.model1[0].id);
        done();
      }).done();
    });

    it('should have set model2[3].test1Id to model1[0].id', function (done) {
      shared.db(Model2.tableName).where('id', models.model2[3].id).then(function (rows) {
        expect(rows[0].test1Id).to.eql(models.model1[0].id);
        done();
      }).done();
    });

    it('should have created 2 rows in Test1_Test2 join table', function (done) {
      shared.db('Test1_Test2').then(function (rows) {
        expect(rows).to.have.length(2);
        expect(rows[0].test1Id).to.eql(models.model1[0].id);
        expect(rows[0].test2Id).to.eql(models.model2[4].id);
        expect(rows[1].test1Id).to.eql(models.model1[0].id);
        expect(rows[1].test2Id).to.eql(models.model2[5].id);
        done();
      }).done();
    });

    it('should have created 4 rows in joinTableForTest2AndTest3 join table', function (done) {
      shared.db('joinTableForTest2AndTest3').then(function (rows) {
        expect(rows).to.have.length(4);
        expect(rows[0].thisIsTest2Id).to.eql(models.model2[4].id);
        expect(rows[0].andThisIsTest3Id).to.eql(models.model3[1].id);
        expect(rows[1].thisIsTest2Id).to.eql(models.model2[5].id);
        expect(rows[1].andThisIsTest3Id).to.eql(models.model3[3].id);
        expect(rows[2].thisIsTest2Id).to.eql(models.model2[5].id);
        expect(rows[2].andThisIsTest3Id).to.eql(models.model3[4].id);
        expect(rows[3].thisIsTest2Id).to.eql(models.model2[5].id);
        expect(rows[3].andThisIsTest3Id).to.eql(models.model3[5].id);
        done();
      }).done();
    });

  });

  function insertWithInstanceInsert(modelToInsert) {
    return modelToInsert.$insert().then(function (model) {
      expect(model.id).to.not.eql(null);
      expect(model.$toJson(true)).to.eql(modelToInsert.$toJson(true));
      // Check that the database actually contains the inserted model.
      return shared.db(modelToInsert.constructor.tableName).where('id', model.id);
    }).then(function (rows) {
      expect(rows[0]).to.eql(modelToInsert.$toDatabaseJson());
    });
  }

  function insertWithStaticInsert(modelToInsert) {
    return modelToInsert.constructor.insert(modelToInsert).then(function (model) {
      expect(model.id).to.not.eql(null);
      expect(model.$toJson(true)).to.eql(modelToInsert.$toJson(true));
      // Check that the database actually contains the inserted model.
      return shared.db(modelToInsert.constructor.tableName).where('id', model.id);
    }).then(function (rows) {
      expect(rows[0]).to.eql(modelToInsert.$toDatabaseJson());
    });
  }

  function insertWithInsertRelated(parentModel, relationName, modelToInsert) {
    return parentModel.$insertRelated(relationName, modelToInsert).then(function (model) {
      expect(model.id).to.not.eql(null);
      expect(model.$toJson(true)).to.eql(modelToInsert.$toJson(true));
      // Check that the database actually contains the inserted model.
      return shared.db(modelToInsert.constructor.tableName).where('id', model.id);
    }).then(function (rows) {
      expect(rows[0]).to.eql(modelToInsert.$toDatabaseJson());
    });
  }
};
