var _ = require('lodash')
  , expect = require('expect.js')
  , Promise = require('bluebird');


module.exports = function (shared) {

  var Model1 = shared.modelClasses.Model1;
  var Model2 = shared.modelClasses.Model2;
  var Model3 = shared.modelClasses.Model3;
  var models = shared.models;

  describe('#$del()', function () {

    it('should delete model2[1] using instance method', function () {
      return shared.db(Model2.tableName).where('id', models.model2[1].id).then(function (rows) {
        expect(rows).to.have.length(1);
        return models.model2[1].$del();
      }).then(function () {
        return shared.db(Model2.tableName).where('id', models.model2[1].id);
      }).then(function (rows) {
        expect(rows).to.have.length(0);
        return shared.db(Model2.tableName);
      }).then(function (rows) {
        expect(rows).to.have.length(5);
      });
    });

    it('should delete model2[2] using static deleteById method', function () {
      return shared.db(Model2.tableName).where('id', models.model2[2].id).then(function (rows) {
        expect(rows).to.have.length(1);
        return Model2.deleteById(models.model2[2].id);
      }).then(function () {
        return shared.db(Model2.tableName).where('id', models.model2[2].id);
      }).then(function (rows) {
        expect(rows).to.have.length(0);
        return shared.db(Model2.tableName);
      }).then(function (rows) {
        expect(rows).to.have.length(4);
      });
    });

    it('can be chained with where() method', function () {
      return shared.db(Model2.tableName).where('id', models.model2[3].id).then(function (rows) {
        expect(rows).to.have.length(1);
        return Model2.del().where('textProperty2', models.model2[3].textProperty2);
      }).then(function () {
        return shared.db(Model2.tableName).where('id', models.model2[3].id);
      }).then(function (rows) {
        expect(rows).to.have.length(0);
        return shared.db(Model2.tableName);
      }).then(function (rows) {
        expect(rows).to.have.length(3);
        
      });
    });

  });

  describe('#deleteRelated()', function () {

    it('should delete model2[0] (HasOneRelation)', function () {
      return shared.db(Model2.tableName).where('id', models.model2[0].id).then(function (rows) {
        // Test that model2[0] exists.
        expect(rows).to.have.length(1);
        return shared.db(Model1.tableName);
      }).then(function (rows) {
        // Test that model1[0] is bound to model2[0] through 'child' relation.
        expect(rows[0].test2Id).to.equal(models.model2[0].id);
        return models.model1[0].$deleteRelated('child');
      }).then(function () {
        return shared.db(Model2.tableName).where('id', models.model2[0].id);
      }).then(function (rows) {
        // Test that model2[0] has been deleted.
        expect(rows).to.have.length(0);
        return shared.db(Model2.tableName);
      }).then(function (rows) {
        expect(rows).to.have.length(2);
        return shared.db(Model1.tableName);
      }).then(function (rows) {
        // Test that the binding from model1[0] to model2[0] has been removed.
        expect(rows[0].test2Id).to.equal(null);
      });
    });

    it('should delete model3[2] (HasManyRelation)', function () {
      return shared.db(Model3.tableName).where('id', models.model3[2].id).then(function (rows) {
        // Test that model3[2] exists.
        expect(rows).to.have.length(1);
        expect(rows[0].test2Id).to.equal(models.model2[5].id);
        return models.model2[5].$deleteRelated('grandChildren1', models.model3[2].id);
      }).then(function () {
        return shared.db(Model3.tableName).where('id', models.model3[2].id);
      }).then(function (rows) {
        // Test that model3[2] has been deleted.
        expect(rows).to.have.length(0);
        return shared.db(Model3.tableName);
      }).then(function (rows) {
        expect(rows).to.have.length(5);
      });
    });

    it('should delete model3[3] (ManyToManyRelation)', function () {
      return shared.db(Model3.tableName).where('id', models.model3[3].id).then(function (rows) {
        // Test that model3[3] exists.
        expect(rows).to.have.length(1);
        expect(rows[0].test2Id).to.equal(null);
        return shared.db('joinTableForTest2AndTest3')
          .where('thisIsTest2Id', models.model2[5].id)
          .where('andThisIsTest3Id', models.model3[3].id);
      }).then(function (rows) {
        // Test that join table has the join row.
        expect(rows).to.have.length(1);
        return models.model2[5].$deleteRelated('grandChildren2', models.model3[3].id);
      }).then(function (rows) {
        return shared.db(Model3.tableName).where('id', models.model3[3].id);
      }).then(function (rows) {
        // Test that model3[3] has been deleted.
        expect(rows).to.have.length(0);
        return shared.db(Model3.tableName);
      }).then(function (rows) {
        expect(rows).to.have.length(4);
        return shared.db('joinTableForTest2AndTest3')
          .where('thisIsTest2Id', models.model2[5].id)
          .where('andThisIsTest3Id', models.model3[3].id);
      }).then(function (rows) {
        // Test that join table doesn't have the join row.
        expect(rows).to.have.length(0);
      });
    });

  });

};
