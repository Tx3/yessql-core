var _ = require('lodash')
  , expect = require('expect.js')
  , Promise = require('bluebird');

module.exports = function (shared) {

  var Model1 = shared.modelClasses.Model1;
  var models = shared.models;

  describe('#update()', function () {

    it('should update model1[0] (instance)', function () {
      models.model1[0].numberProperty = 21234314;
      return models.model1[0].$update().then(function (model) {
        expect(model.$toJson(true)).to.eql(models.model1[0].$toJson(true));
        return shared.db(Model1.tableName).where('id', models.model1[0].id);
      }).then(function (rows) {
        expect(rows[0]).to.eql(models.model1[0].$toDatabaseJson());
        expect(rows[0].numberProperty).to.eql(21234314);
      });
    });

    it('should work even if nothing gets updated', function () {
      return models.model1[0].$update().then(function (model) {
        expect(model.$toJson(true)).to.eql(models.model1[0].$toJson(true));
        return shared.db(Model1.tableName).where('id', models.model1[0].id);
      }).then(function (rows) {
        expect(rows[0]).to.eql(models.model1[0].$toDatabaseJson());
        expect(rows[0].numberProperty).to.eql(21234314);
      });
    });

    it('can be chained with eager() method (instance)', function () {
      models.model1[0].numberProperty = 1;
      var send = models.model1[0].$clone();
      delete send.child;
      delete send.customChild;
      delete send.children1;
      delete send.children2;
      delete send.children1WithQuery;
      return send.$update().eager('child').then(function (model) {
        expect(_.keys(model)).to.contain('child');
        expect(_.keys(model)).to.not.contain('customChild');
        expect(_.keys(model)).to.not.contain('children1');
        expect(_.keys(model)).to.not.contain('children2');
        expect(_.keys(model)).to.not.contain('children1WithQuery');
        expect(model.child).to.eql(models.model2[0]);
        expect(model.$toJson(true)).to.eql(models.model1[0].$toJson(true));
        return shared.db(Model1.tableName).where('id', models.model1[0].id);
      }).then(function (rows) {
        expect(rows[0]).to.eql(models.model1[0].$toDatabaseJson());
        expect(rows[0].numberProperty).to.eql(1);
      });
    });

    it('should update model1[0] (static)', function () {
      models.model1[0].numberProperty = 3;
      return Model1.update(models.model1[0]).then(function (model) {
        expect(model.$toJson(true)).to.eql(models.model1[0].$toJson(true));
        return shared.db(Model1.tableName).where('id', models.model1[0].id);
      }).then(function (rows) {
        expect(rows[0]).to.eql(models.model1[0].$toDatabaseJson());
        expect(rows[0].numberProperty).to.eql(3);
      });
    });

    it('should return null if no rows are affected (static)', function () {
      var model = models.model1[0].$clone();
      model.id = Model1.generateId();
      return Model1.update(model).then(function (model) {
        expect(model).to.eql(null);
        return shared.db(Model1.tableName).where('id', models.model1[0].id);
      }).then(function (rows) {
        expect(rows[0]).to.eql(models.model1[0].$toDatabaseJson());
        expect(rows[0].numberProperty).to.eql(3);
      });
    });

  });


  describe('#unbindRelated', function () {

    it('should unbind model1[0].child', function () {
      return models.model1[0].$unbindRelated('child').then(function () {
        return shared.db(Model1.tableName).where('id', models.model1[0].id);
      }).then(function (rows) {
        expect(rows).to.have.length(1);
        expect(rows[0].test2Id).to.eql(null);
      });
    });

    it('should unbind model1[0].children2[0]', function () {
      return models.model1[0].$unbindRelated('children2', models.model2[4].id).then(function () {
        return shared.db('Test1_Test2').where('test2Id', models.model2[4].id);
      }).then(function (rows) {
        expect(rows).to.have.length(0);
      });
    });

  });


  describe('#bindRelated', function () {

    it('should bind model2[0] to model1[0].child', function () {
      return models.model1[0].$bindRelated('child', models.model2[0].id).then(function () {
        return shared.db(Model1.tableName).where('id', models.model1[0].id);
      }).then(function (rows) {
        expect(rows).to.have.length(1);
        expect(rows[0].test2Id).to.eql(models.model2[0].id);
      });
    });

    it('should bind model2[4] to model1[0].children2', function () {
      return models.model1[0].$bindRelated('children2', models.model2[4].id).then(function () {
        return shared.db('Test1_Test2').where('test2Id', models.model2[4].id);
      }).then(function (rows) {
        expect(rows).to.have.length(1);
      });
    });

  });

};
