var _ = require('lodash')
  , expect = require('expect.js')
  , DatabaseManager = require('../../database/db-utils')
  , Promise = require('bluebird');

Promise.longStackTraces();

var postgresConf = {
  client: 'postgres',
  host: 'localhost',
  database: 'dbmanger-test-database-deleteme',
  // currently fail if these not found from postgres
  collate: ['fi_FI.UTF-8', 'Finnish_Finland.1252'],
  user: 'postgres',
  password: undefined,
  superUser: 'postgres',
  superPassword: undefined,
  minConnectionPoolSize: 0,
  maxConnectionPoolSize: 10,
  migrationsDir: __dirname + '/migrations',
  migrationsTable: 'testmigrations'
};

var mysqlConf = { };

var sqliteConf = { };

/**
 * All tests depends that the ones ran earlier were success.
 */
describe('DatabaseManager', function() {

  var availableDatabases = [ new DatabaseManager(postgresConf) ];
  var dbCopyName = 'dbmanger-test-database-copy-deleteme';

  before(function () {
    // Make sure that database does not exist
    return Promise.all(
      _.map(availableDatabases, function (dbManager) {
        return Promise.all([
          dbManager.dropDb(dbManager.config.database),
          dbManager.dropDb(dbCopyName)
        ]);
      })
    );
  });

  it("#knexInstance should fail to create knex a instance with non existing db", function () {
    return Promise.all(
      _.map(availableDatabases, function (dbManager) {
        var knex = dbManager.knexInstance(dbManager.config.database);
        return knex.raw(';')
          .then(function () {
            expect("Expected error from DB").to.fail();
          }).catch(function () {
            expect("All good!").to.be.ok();
          })
          .then(function () {
            knex.destroy();
          });
      })
    );
  });

  it("#createDb should create a database", function () {
    return Promise.all(
      _.map(availableDatabases, function (dbManager) {
        return dbManager.createDb(dbManager.config.database)
          .then(function () {
            // connecting db should work
            var knex = dbManager.knexInstance(dbManager.config.database);
            return knex.raw(';').then(function () {
              return knex.destroy();
            });
          });
      }));
  });

  it("#migrateDb should update version and run migrations", function () {
    return Promise.all(_.map(availableDatabases, function (dbManager) {
      return dbManager.dbVersion(dbManager.config.database)
        .then(function (originalVersionInfo) {
          expect(originalVersionInfo).to.be('none');
          return dbManager.migrateDb(dbManager.config.database);
        })
        .then(function (migrateResponse) {
          expect(migrateResponse[0]).to.be(1);
          return dbManager.dbVersion(dbManager.config.database);
        })
        .then(function (versionInfo) {
          expect(versionInfo).to.be('20141024070315');
          return dbManager.migrateDb(dbManager.config.database);
        })
        .then(function (migrateResponse) {
          expect(migrateResponse[0]).to.be(2);
          return dbManager.migrateDb(dbManager.config.database);
        })
        .then(function (migrateResponse) {
          expect(migrateResponse[0]).to.be(2);
          return dbManager.dbVersion(dbManager.config.database);
        })
        .then(function (versionInfo) {
          expect(versionInfo).to.be('20141024070315');
          return dbManager.migrateDb(dbManager.config.database);
        });
      }));
  });

  it("#populateDb should populate data from given directory", function () {
    return Promise.all(
      _.map(availableDatabases, function (dbManager) {
        return dbManager.populateDb(dbManager.config.database, __dirname + '/populate/*.js')
          .then(function () {
            var knex = dbManager.knexInstance(dbManager.config.database);
            return knex.select().from('User').then(function (result) {
              expect(result[0].id).to.be('1');
            }).then(function () {
              return knex.destroy();
            });
          });
      }));
  });

  it("#copyDb should copy a database", function () {
    return Promise.all(
      _.map(availableDatabases, function (dbManager) {
        return dbManager.copyDb(dbManager.config.database, dbCopyName)
          .then(function () {
            var knex = dbManager.knexInstance(dbCopyName);
            return knex.select().from('User')
              .then(function (result) {
                expect(result[0].id).to.be('1');
              })
              .then(function () {
                return knex.destroy();
              });
          });
      }));
  });

  it("#truncateDb should truncate a database", function () {
    return Promise.all(_.map(availableDatabases, function (dbManager) {
      return dbManager.truncateDb(dbManager.config.database)
        .then(function (result) {
          var knex = dbManager.knexInstance(dbManager.config.database);

          return Promise.all([
            knex.select().from('User').then(function (result) {
              expect(result.length).to.be(0);
            }),
            dbManager.dbVersion(dbManager.config.database).then(function (ver) {
              expect(ver).to.be('20141024070315');
            }),
            knex('User').insert({
              username: 'new',
              email: 'imtadmin@fake.invalid'
            }).then(function () {
              return knex.select().from('User');
            }).then(function (result) {
              expect(result[0].id).to.be('1');
            })
          ])
          .then(function () {
            return knex.destroy();
          });
        });
    }));
  });

  it("#dropDb should drop a database", function () {
    return Promise.all(
      _.map(availableDatabases, function (dbManager) {
        return Promise.all([
          dbManager.dropDb(dbManager.config.database),
          dbManager.dropDb(dbCopyName),
          dbManager.dropDb(dbCopyName) // this should not fail
        ]).then(function () {
          // test db was dropped
          var knex = dbManager.knexInstance(dbManager.config.database);
          return knex.raw(';').then(function () {
            expect("Expected error from DB").to.fail();
          })
          .catch(function (err) {
            expect("All good!").to.be.ok();
          })
          .then(function () {
            knex.destroy();
          });

        }).then(function () {
          // copy db was dropped
          var knex = dbManager.knexInstance(dbCopyName);
          return knex.raw(';').then(function () {
            expect("Expected error from DB").to.fail();
          })
          .catch(function () {
            expect("All good!").to.be.ok();
          })
          .then(function () {
            knex.destroy();
          });
        });
      }));
  });

});
